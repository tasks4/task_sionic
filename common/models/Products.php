<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $weight
 * @property string|null $usage
 * @property string|null $vendor_code
 * @property string|null $price_msk
 * @property string|null $quantity_msk
 * @property string|null $price_spb
 * @property string|null $quantity_spb
 * @property string|null $price_samara
 * @property string|null $quantity_samara
 * @property string|null $price_saratov
 * @property string|null $quantity_saratov
 * @property string|null $price_kazan
 * @property string|null $quantity_kazan
 * @property string|null $price_nvs
 * @property string|null $quantity_nvs
 * @property string|null $price_che
 * @property string|null $quantity_che
 * @property string|null $price_linii_chelyabinsk
 * @property string|null $quantity_linii_chelyabinsk
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usage'], 'string'],
            [['name', 'weight', 'vendor_code', 'price_msk', 'quantity_msk', 'price_spb', 'quantity_spb', 'price_samara', 'quantity_samara', 'price_saratov', 'quantity_saratov', 'price_kazan', 'quantity_kazan', 'price_nvs', 'quantity_nvs', 'price_che', 'quantity_che', 'price_linii_chelyabinsk', 'quantity_linii_chelyabinsk'], 'string', 'max' => 800],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'weight' => Yii::t('app', 'Weight'),
            'usage' => Yii::t('app', 'Usage'),
            'vendor_code' => Yii::t('app', 'Vendor Code'),
            'price_msk' => Yii::t('app', 'Price Msk'),
            'quantity_msk' => Yii::t('app', 'Quantity Msk'),
            'price_spb' => Yii::t('app', 'Price Spb'),
            'quantity_spb' => Yii::t('app', 'Quantity Spb'),
            'price_samara' => Yii::t('app', 'Price Samara'),
            'quantity_samara' => Yii::t('app', 'Quantity Samara'),
            'price_saratov' => Yii::t('app', 'Price Saratov'),
            'quantity_saratov' => Yii::t('app', 'Quantity Saratov'),
            'price_kazan' => Yii::t('app', 'Price Kazan'),
            'quantity_kazan' => Yii::t('app', 'Quantity Kazan'),
            'price_nvs' => Yii::t('app', 'Price Nvs'),
            'quantity_nvs' => Yii::t('app', 'Quantity Nvs'),
            'price_che' => Yii::t('app', 'Price Che'),
            'quantity_che' => Yii::t('app', 'Quantity Che'),
            'price_linii_chelyabinsk' => Yii::t('app', 'Price Linii Chelyabinsk'),
            'quantity_linii_chelyabinsk' => Yii::t('app', 'Quantity Linii Chelyabinsk'),
        ];
    }

    public function behaviors()
    {
        return[
            \kozhemin\dbHelper\InsertUpdate::class,
        ];
    }
}
