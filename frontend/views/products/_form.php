<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'usage')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'vendor_code')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_msk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_msk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_spb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_spb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_samara')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_samara')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_saratov')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_saratov')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_kazan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_kazan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_nvs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_nvs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_che')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_che')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price_linii_chelyabinsk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity_linii_chelyabinsk')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
