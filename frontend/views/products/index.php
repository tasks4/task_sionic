<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'products');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="products-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create products'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['style' => 'width:100%'],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
        'name',
            'weight',
            'usage:ntext',
            'vendor_code',
            'price_msk',
            'quantity_msk',
            'price_spb',
            'quantity_spb',
            'price_samara',
            'quantity_samara',
            'price_saratov',
            'quantity_saratov',
            'price_kazan',
            'quantity_kazan',
            'price_nvs',
            'quantity_nvs',
            'price_che',
            'quantity_che',
            'price_linii_chelyabinsk',
            'quantity_linii_chelyabinsk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
