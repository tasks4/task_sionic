<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\ProductsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'weight') ?>

    <?= $form->field($model, 'usage') ?>

    <?= $form->field($model, 'vendor_code') ?>

    <?php // echo $form->field($model, 'price_msk') ?>

    <?php // echo $form->field($model, 'quantity_msk') ?>

    <?php // echo $form->field($model, 'price_spb') ?>

    <?php // echo $form->field($model, 'quantity_spb') ?>

    <?php // echo $form->field($model, 'price_samara') ?>

    <?php // echo $form->field($model, 'quantity_samara') ?>

    <?php // echo $form->field($model, 'price_saratov') ?>

    <?php // echo $form->field($model, 'quantity_saratov') ?>

    <?php // echo $form->field($model, 'price_kazan') ?>

    <?php // echo $form->field($model, 'quantity_kazan') ?>

    <?php // echo $form->field($model, 'price_nvs') ?>

    <?php // echo $form->field($model, 'quantity_nvs') ?>

    <?php // echo $form->field($model, 'price_che') ?>

    <?php // echo $form->field($model, 'quantity_che') ?>

    <?php // echo $form->field($model, 'price_linii_chelyabinsk') ?>

    <?php // echo $form->field($model, 'quantity_linii_chelyabinsk') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
