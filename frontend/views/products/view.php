<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'weight',
            'usage:ntext',
            'vendor_code',
            'price_msk',
            'quantity_msk',
            'price_spb',
            'quantity_spb',
            'price_samara',
            'quantity_samara',
            'price_saratov',
            'quantity_saratov',
            'price_kazan',
            'quantity_kazan',
            'price_nvs',
            'quantity_nvs',
            'price_che',
            'quantity_che',
            'price_linii_chelyabinsk',
            'quantity_linii_chelyabinsk',
        ],
    ]) ?>

</div>
