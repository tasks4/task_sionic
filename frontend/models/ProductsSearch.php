<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

/**
 * ProductsSearch represents the model behind the search form of `common\models\products`.
 */
class ProductsSearch extends Products
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'weight', 'usage', 'vendor_code', 'price_msk', 'quantity_msk', 'price_spb', 'quantity_spb', 'price_samara', 'quantity_samara', 'price_saratov', 'quantity_saratov', 'price_kazan', 'quantity_kazan', 'price_nvs', 'quantity_nvs', 'price_che', 'quantity_che', 'price_linii_chelyabinsk', 'quantity_linii_chelyabinsk'], 'trim'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'weight', $this->weight])
            ->andFilterWhere(['like', 'usage', $this->usage])
            ->andFilterWhere(['like', 'vendor_code', $this->vendor_code])
            ->andFilterWhere(['like', 'price_msk', $this->price_msk])
            ->andFilterWhere(['like', 'quantity_msk', $this->quantity_msk])
            ->andFilterWhere(['like', 'price_spb', $this->price_spb])
            ->andFilterWhere(['like', 'quantity_spb', $this->quantity_spb])
            ->andFilterWhere(['like', 'price_samara', $this->price_samara])
            ->andFilterWhere(['like', 'quantity_samara', $this->quantity_samara])
            ->andFilterWhere(['like', 'price_saratov', $this->price_saratov])
            ->andFilterWhere(['like', 'quantity_saratov', $this->quantity_saratov])
            ->andFilterWhere(['like', 'price_kazan', $this->price_kazan])
            ->andFilterWhere(['like', 'quantity_kazan', $this->quantity_kazan])
            ->andFilterWhere(['like', 'price_nvs', $this->price_nvs])
            ->andFilterWhere(['like', 'quantity_nvs', $this->quantity_nvs])
            ->andFilterWhere(['like', 'price_che', $this->price_che])
            ->andFilterWhere(['like', 'quantity_che', $this->quantity_che])
            ->andFilterWhere(['like', 'price_linii_chelyabinsk', $this->price_linii_chelyabinsk])
            ->andFilterWhere(['like', 'quantity_linii_chelyabinsk', $this->quantity_linii_chelyabinsk]);

        return $dataProvider;
    }
}
