<?php

namespace console\controllers;

use common\models\Products;
use console\components\Parser;

class ParserController extends \yii\console\Controller
{

    public function actionIndex()
    {
        $startTime = microtime(true);

        $model = new Products();
        $parser = new Parser();
        $products = $parser->receiveProducts(\Yii::getAlias('@frontend') . '/web/data');

        echo "\nЗапис в База Даних";

        $startTimeDB = microtime(true);
        $model->InsertUpdate($products, $model->attributes());
        $endTimeDB = microtime(true) - $startTimeDB;

        echo sprintf("-> %s \n", $endTimeDB);

        $endTime = microtime(true) - $startTime;
        echo sprintf("\n\nTotal - %s \n\n", $endTime);

        return true;
    }
}