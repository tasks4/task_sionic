<?php

namespace console\components;

use common\models\Products;


class Parser extends \yii\base\Component
{
    /**
     * @var array
     */
    private $products = [];

    private const CITES =
        [
            'Москва' => 'msk',
            'Санкт-Петербург' => 'spb',
            'Самара' => 'samara',
            'Саратов' => 'saratov',
            'Казань' => 'kazan',
            'Новосибирск' => 'nvs',
            'Челябинск' => 'che',
            'Деловые линии Челябинск' => 'linii_chelyabinsk',
        ];


    /**
     * @param $path
     * @return array
     */
    public function run(string $path)
    {
        $this->receiveProducts($path);

        return $this->products;
    }

    /**
     * @param string $path
     * @return array
     */
    public function receiveProducts(string $path): array
    {
        echo "Собранный данные из XML\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

        for ($i = 0; $i < 8; $i++) {
            $pathImport = sprintf('%s/import%s_1.xml', $path, $i);
            $pathOffers = sprintf('%s/offers%s_1.xml', $path, $i);
            $startTime = microtime(true);

            $importData = $this->receiveFile($pathImport);
            $cityKey = $this->receiveCity($importData);

            echo $cityKey;

            $this->receiveImportData($importData);

            $offersData = $this->receiveFile($pathOffers);
            $city = self::CITES[$cityKey];

            $this->receiveOffersData($offersData, $city);

            $endTime = microtime(true) - $startTime;

            echo sprintf(" -> %s \n", $endTime);
        }

        return $this->products;
    }

    /**
     * @param string $path
     * @return array
     */
    public function receiveFile(string $path): array
    {
        return
            json_decode(
                json_encode(
                    simplexml_load_string(file_get_contents($path))
                ),
                true
            );
    }


    /**
     * @param string $source
     * @return string
     */
    private function receiveCity(array $source): string
    {
        return
            trim(
                preg_replace(
                    '~.*\(|\).*~sui',
                    '',
                    $source['Классификатор']['Наименование'] ?? ''
                )
            );
    }

    /**
     * @param array $source
     * @return mixed[]
     */
    private function receiveImportData(array $source)
    {
        foreach ($source['Каталог']['Товары']['Товар'] as $index => $product) {
            if (empty($product['Наименование'])) {
                continue;
            }

            $productData = $this->products[$product['Код']] ?? [];

            $this->products[$product['Код']] =
                [
                    'id' => $product['Код'],
                    'name' => $product['Наименование'],
                    'weight' => $product['Вес'] ?? '',
                    'usage' => $this->receiveUsage($product),
                    'vendor_code' => $productData['vendor_code'] ?? '',
                    'price_msk' => $productData['price_msk'] ?? '',
                    'quantity_msk' => $productData['quantity_msk'] ?? '',
                    'price_spb' => $productData['price_spb'] ?? ' ',
                    'quantity_spb' => $productData['quantity_spb'] ?? '',
                    'price_samara' => $productData['price_samara'] ?? '',
                    'quantity_samara' => $productData['quantity_samara'] ?? '',
                    'price_saratov' => $productData['price_saratov'] ?? ' ',
                    'quantity_saratov' => $productData['quantity_saratov'] ?? '',
                    'price_kazan' => $productData['price_kazan'] ?? ' ',
                    'quantity_kazan' => $productData['quantity_kazan'] ?? '',
                    'price_nvs' => $productData['price_nvs'] ?? '',
                    'quantity_nvs' => $productData['quantity_nvs'] ?? '',
                    'price_che' => $productData['price_che'] ?? '',
                    'quantity_che' => $productData['quantity_che'] ?? '',
                    'price_linii_chelyabinsk' => $productData['price_linii_chelyabinsk'] ?? '',
                    'quantity_linii_chelyabinsk' => $productData['quantity_linii_chelyabinsk'] ?? '',
                ];
        }

        return $this->products;
    }

    /**
     * @param array $product
     * @return string
     */
    private function receiveUsage(array $product): string
    {
        $usage = '';

        if (empty($product['Взаимозаменяемости']['Взаимозаменяемость'])) {
            return $usage;
        }

        foreach ($product['Взаимозаменяемости']['Взаимозаменяемость'] as $index => $interchangeability) {
            $checkUsage = (
                empty($interchangeability['Марка'])
                or empty($interchangeability['Модель'])
                or empty($interchangeability['КатегорияТС'])
            );

            if ($checkUsage) {
                continue;
            }

            $usage = sprintf('%s|%s', $usage, implode('-', $interchangeability));
        }

        return trim($usage, '|');
    }

    /**
     * @param array $source
     * @param string $city
     * @return mixed[]
     */
    private function receiveOffersData(array $source, string $city)
    {
        foreach ($source['ПакетПредложений']['Предложения']['Предложение'] as $offer) {
            if (empty($this->products[$offer['Код']])) {
                continue;
            }

            if (is_array($offer['Артикул'] ?? '')) {
                $offer['Артикул'] = '';
            }

            $this->products[$offer['Код']]['price_' . $city] = $offer['Цены']['Цена'][0]['ЦенаЗаЕдиницу'] ?? 0;
            $this->products[$offer['Код']]['vendor_code'] = $offer['Артикул'] ?? '';
            $this->products[$offer['Код']]['quantity_' . $city] = $offer['Количество'] ?? '';
        }

        return $this->products;
    }
}

