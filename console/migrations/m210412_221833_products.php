<?php

use yii\db\Migration;

/**
 * Class m210412_221833_products
 */
class m210412_221833_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210412_221833_products cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(800),
            'weight'=>$this->string(800),
            'usage'=>$this->text(),
            'vendor_code'=>$this->string(800),
            'price_msk'=>$this->string(800),
            'quantity_msk'=>$this->string(800),
            'price_spb'=>$this->string(800),
            'quantity_spb'=>$this->string(800),
            'price_samara'=>$this->string(800),
            'quantity_samara'=>$this->string(800),
            'price_saratov'=>$this->string(800),
            'quantity_saratov'=>$this->string(800),
            'price_kazan'=>$this->string(800),
            'quantity_kazan'=>$this->string(800),
            'price_nvs'=>$this->string(800),
            'quantity_nvs'=>$this->string(800),
            'price_che'=>$this->string(800),
            'quantity_che'=>$this->string(800),
            'price_linii_chelyabinsk'=>$this->string(800),
            'quantity_linii_chelyabinsk'=>$this->string(800),

        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%products}}');
    }
}
